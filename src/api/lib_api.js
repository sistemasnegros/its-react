import axios from 'axios';

// Funcion que para obtener datos de una api
// function getDataApi (url, funcion_response, function_error){
//   axios.get(url)
//     .then( response => {

//       //console.log(response.data);

//       // Ejecuto la funcion despues de recibir la respesta del servidor
//       funcion_response(response);
//       //this.setState({products: response.data});

//     })
//     .catch( error => {
//       console.log(error);

//     });

// }


const getDataApi = (url, funcion_response, function_error) => {
  axios.get(url)
    .then( response => {

      //console.log(response.data);

      // Ejecuto la funcion despues de recibir la respesta del servidor
      funcion_response(response);
      //this.setState({products: response.data});

    })
    .catch( error => {
      console.log(error);

    });

};



function sendDataAPI(data, url){
  axios.post(url, data)
    .then(response => {
      console.log(response);
    })
    .catch(error => {
      console.log(error);
    });

}


export {getDataApi, sendDataAPI};
