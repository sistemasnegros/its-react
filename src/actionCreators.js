import axios from 'axios';

const apiUrl = {
  create: 'http://localhost:3000/api/create-data',
  update: 'http://localhost:3000/api/update-data',
  get: 'http://localhost:3000/api/get-data'
};

const loadProducts = () => {
  return dispatch => {
    return axios.get(apiUrl.get)
      .then( response => {
        dispatch({
          type: "REPLACE_PRODUCTS",
          products: response.data
        });
      });
  };
};

const editToProduct = product => {
  return dispatch => {
    return axios.post(apiUrl.update, product)
      .then(response => {
        console.log(response);
        dispatch({
          type: "EDIT_TO_PRODUCT",
          product: product
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
  // return {
  //   type:"EDIT_TO_PRODUCT",
  //   product: product
  // };
};

const addToCart = product => {
  return {
    type:"ADD_TO_CART",
    product: product
  };
};



const removeFromCart = product => {
  return {
    type: "REMOVE_FROM_CART",
    product:product
  };

};


const showMenu = element => {
  return {
    type: "SHOW_MENU",
    element : element
  };

};

export {addToCart, removeFromCart, loadProducts, editToProduct, showMenu };
