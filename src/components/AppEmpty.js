
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//import { Link } from 'react-router-dom';


// Barra de navegacion
//import NavBar from './example/NavBar'
//import MiniNavBar from './example/NavBar'
import NavBar from './base/NavBar';
//import MiniNavBar from './example/base/NavBar/MiniNavBar';

// Estilos Contenedor prinipal
//import '../index.css';


class AppEmpty extends Component {
  //static propTypes = {children: PropTypes.object.isRequired};

  //<MiniNavBar/>
  render() {
    const { children } = this.props;

    //<h1>-Este es mi cabecera</h1>
    return (
      <div className="App">

        <NavBar/>
        <div id="content">
          {children}
          <footer>-Este es el footer</footer>
        </div>

      </div>
    );
  }
}

AppEmpty.protoTypes = {
  children: PropTypes.object.isRequired,
};

export default AppEmpty;
