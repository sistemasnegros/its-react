import React, { Component } from 'react';
import axios from 'axios';

import {Table, Button} from 'react-bootstrap';




class DataTable extends Component {
  constructor(props) {
    super(props);

    this.state ={
      product: []
    };


    //var self = this;

    axios.get('http://localhost:3000/api/get-data')
      .then( response => {

        console.log(response.data);
        this.setState({product: response.data});

      })
      .catch( error => {

        console.log(error);

      });




  }

  componentWillMount() {

  }

  render() {

    return (
      <div>
        <h1>DataTable</h1>
        <Table striped bordered condensed hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Precio</th>
              <th>Edit</th>
              <th>Remove</th>

            </tr>
          </thead>
          <tbody>
            {this.state.product.map( product =>
              <tr key={product.id}>
                <td>{product.id}</td>
                <td>{product.nombre}</td>
                <td>${product.precio}</td>
                <td><Button className="btn btn-primary btn-large centerButton">Edit</Button></td>
                <td><Button className="btn btn-primary btn-large centerButton">Delete</Button></td>

              </tr>
            )}
          </tbody>
        </Table>


      </div>
    );
  }
}

export default DataTable;
