// Dependencies
import React, { Component } from 'react';
import {Form, FormGroup, Col, ControlLabel, FormControl,Checkbox ,Button } from 'react-bootstrap';
import axios from 'axios';

const style = {
  div : {
    margin: '0 auto',
    width:'60%'

  }


};
class FormLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };

    //this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // handleChange(event) {
  //     this.setState({value: event.target.value});
  // }

  handleEmailChange(e) {
    this.setState({username: e.target.value});
  }


  handlePasswordChange(e) {
    this.setState({password: e.target.value});
  }

  handleSubmit(event) {
    //alert('A name was submitted: ' + this.state.value);
    console.log(this.state);
    event.preventDefault();

    this.sendDataAPI(this.state);

  }

  sendDataAPI(data){
    axios.post('http://localhost:3000/api/login', data)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });

  }


  render() {
    return (
      <div style={style.div}>
        <form onSubmit={this.handleSubmit} >
          <FormGroup role="form">
            <FormControl type="text" placeholder="Username"  className="form-control" value={this.state.username} onChange={this.handleEmailChange.bind(this)}/>
            <FormControl type="password" placeholder="Password" className="form-control" value={this.state.password} onChange={this.handlePasswordChange.bind(this)}/>
            <Button className="btn btn-primary btn-large centerButton" type="submit">Send</Button>
          </FormGroup>
        </form>
      </div>

    );
  }
}

export default FormLogin;
