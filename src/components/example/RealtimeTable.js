import React, { Component } from 'react';
import axios from 'axios';

//import {Table, Button} from 'react-bootstrap';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

const apiUrl = {
  create: 'http://localhost:3000/api/create-data',
  update: 'http://localhost:3000/api/update-data',
  get: 'http://localhost:3000/api/get-data'
};

class RealtimeTable extends Component {
  constructor(props) {
    super(props);

    this.state ={
      products: []
    };


    // Cargo la data de la api
    this.getDataApi(apiUrl.get);

  }



  getDataApi(url){
    axios.get(url)
      .then( response => {

        //console.log(response.data);
        this.setState({products: response.data});

      })
      .catch( error => {

        //console.log(error);

      });

  }

  sendDataAPI(data, url){
    axios.post(url, data)
      .then(response => {
        console.log(response);



      })
      .catch(error => {
        console.log(error);
      });

  }

  onAfterSaveCell(row, cellName, cellValue) {
    //alert(`Save cell ${cellName} with value ${cellValue}`);

    let data = {};

    //let rowStr = '';
    for (const prop in row) {

      data[prop] = row[prop];

      //rowStr += prop + ': ' + row[prop] + '\n';
    }

    // Actualizo cambias en el servidor
    this.sendDataAPI(data, apiUrl.update);

    //alert('Thw whole row :\n' + rowStr);
    //alert(data.id);
  }

  // Insertar fila
  onAfterInsertRow(row) {
    //let newRowStr = '';

    let data = {};
    for (const prop in row) {
      data[prop] = row[prop];
      //newRowStr += prop + ': ' + row[prop] + ' \n';
    }
    //alert('The new row is:\n ' + data);



    this.sendDataAPI(data, apiUrl.create);

    // Cargo la data de la api
    this.getDataApi(apiUrl.get);



  }

  componentWillMount() {
  }

  render() {





    function onBeforeSaveCell(row, cellName, cellValue) {
      // You can do any validation on here for editing value,
      // return false for reject the editing
      return true;
    }

    const cellEditProp = {
      mode: 'dbclick',
      blurToSave: true,
      beforeSaveCell: onBeforeSaveCell, // a hook for before saving cell
      afterSaveCell: this.onAfterSaveCell.bind(this) // a hook for after saving cell

    };

    const options = {
      afterInsertRow: this.onAfterInsertRow.bind(this)  // A hook for after insert rows
    };

    const selectRowProp = {
      mode: 'checkbox'
    };

    return (
      <div>
        <h1>RealtimeTable</h1>

        <BootstrapTable data={ this.state.products } options={ options } cellEdit={ cellEditProp } insertRow={ true } deleteRow={ true } selectRow={ selectRowProp }>
          <TableHeaderColumn dataField='id' autoValue isKey>Product ID</TableHeaderColumn>
          <TableHeaderColumn dataField='nombre' editable={ { type: 'textarea' } } >Product Name</TableHeaderColumn>
          <TableHeaderColumn dataField='precio' editable={ { type: 'textarea' } } >Product Price</TableHeaderColumn>
          <TableHeaderColumn dataField='imagen' editable={ { type: 'textarea' } } >Product Price</TableHeaderColumn>
        </BootstrapTable>


      </div>
    );
  }
}

export default RealtimeTable;
