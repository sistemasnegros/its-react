// Dependencies
import React, { Component } from 'react';

import ProductList from './ProductList';
import ShopingCar from './ShopingCar';

import {Grid, Row, Col} from 'react-bootstrap';

class EcomerceView extends Component {


  render() {


    return (

      <div>
        <h1> Ecomerce </h1>
        <Grid>
          <Row>
            <Col sm={8}>
              <ProductList/>
            </Col>
            <Col sm={4}>
              <ShopingCar/>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default EcomerceView;
