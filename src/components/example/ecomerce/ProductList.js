// Dependencies
import React, { Component } from 'react';
import {Grid, Row, Col, Thumbnail, Button, Image, Glyphicon } from 'react-bootstrap';
import {getDataApi} from '../../../api/lib_api';
import store from '../../../store';
import { addToCart }  from '../../../actionCreators';
import {connect } from 'react-redux';




const styles ={
  product:{
    width:"30%",


    marginLeft: 10,
    marginRight:10


  },
  imagen:{
    height:"200px",
    width:"200px",
    //heigth:"70%",


  },
  flex:{
    display:"flex",
    alignItems: "stretch",
    flexWrap: "wrap",

  }


};

const ProductList =  (props) => {

  return (
    <div>
      <Grid>
        <Row className="show-grid">
          <Col sm={8} >
            <div style={styles.flex}>
              {props.products.map(product =>
                <div key={product.id} style={styles.product}>

                  <img src={product.imagen} style={styles.imagen}  className="img-thumbnail"/>


                  <h3>{product.nombre}</h3>
                  <p>Precio: ${product.precio} </p>
                  <p>
                    <Button bsStyle="primary" onClick={() => props.addToCart(product)}>Comprar <Glyphicon glyph="shopping-cart" />  </Button>&nbsp;

                  </p>
                </div>
              )}
            </div>
          </Col>
        </Row>
      </Grid>
    </div>

  );

};

// Mapeo la el estado a las propiedades al componente pesentacional
const mapStateToProps = state => {
  return {
    products: state.products
  };

};
// Mapeo la el las acciones a las propiedades al componente pesentacional
const mapDispatchToProps = dispatch => {
  return {
    addToCart(product){
      dispatch(addToCart(product));
    }

  };
};

//export default ShopingCar;
export default connect(mapStateToProps, mapDispatchToProps)(ProductList) ;
