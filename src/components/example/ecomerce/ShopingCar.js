// Dependencies
//import React, { Component } from 'react';
import React from 'react';

import {Panel, ListGroup, ListGroupItem, Glyphicon, Button} from 'react-bootstrap';

//import store from '../../../store';

import { removeFromCart }  from '../../../actionCreators';

import {connect } from 'react-redux';

//class ShopingCar extends Component {
// constructor(props) {
//   super(props);


//   this.state ={
//     cart: []
//   };


//   //this.update_state = this.update_state.bind(this);

//   // Cargo la data de la api
//   //getDataApi(apiUrl.get, this.update_state);

//   // me suscribo a cambios del componente produc list
//   store.subscribe(() => {
//     this.setState({
//       cart: store.getState().cart
//     });

//   });

// }
const ShopingCar = ({cart, removeFromCart}) => {
  return (
    <div>
      <Panel collapsible defaultExpanded header="Carrito de compras">
        <ListGroup>
          {cart.map(product =>
            <ListGroupItem key={product.id}>
              {product.nombre} ${product.precio}

              <Button bsSize="xsmall" bsStyle="danger" onClick={() => removeFromCart(product)}>
                <Glyphicon glyph="trash" />

              </Button>
            </ListGroupItem>

          )}

        </ListGroup>
          Total:  $0
      </Panel>
    </div>
  );

  // RemoveFromCart(product){
  //   store.dispatch(removeToCart(product));
  // }



};



const mapStateToProps = state => {
  return {
    cart: state.cart
  };

};

const mapDispatchToProps = dispatch => {
  return {
    removeFromCart(product){
      dispatch(removeFromCart(product));
    }

  };
};

//export default ShopingCar;
export default connect(mapStateToProps, mapDispatchToProps)(ShopingCar) ;
