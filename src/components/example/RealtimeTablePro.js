
import React, { Component } from 'react';
import axios from 'axios';

//import {Table, Button} from 'react-bootstrap';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import {getDataApi} from '../../api/lib_api';

//import store from '../../store';
import { editToProduct }  from '../../actionCreators';
import {connect } from 'react-redux';

const apiUrl = {
  create: 'http://localhost:3000/api/create-data',
  update: 'http://localhost:3000/api/update-data',
  get: 'http://localhost:3000/api/get-data'
};

const getProducts = () => {
  return [{
    id :  1,
    name :  "1",
    price :  1

  }];



};



class RemoteStoreCellEdit extends React.Component {
  constructor(props) {
    super(props);
    // this.products = getProducts();
    this.products = props.products;
    //this.products = getDataApi(apiUrl.get, this.update_state);
    //this.update_state = this.update_state.bind(this);

    this.state = {
      data: this.products
    };
  }

  onCellEdit = (row, fieldName, value) => {
  //   let data = {};
  //   let rowStr = '';
  //   for (const prop in row) {
  //     data[prop] = row[prop];
  //     rowStr += prop + ': ' + row[prop] + '\n';
  //   }
  //   console.log(data);
  //   alert(rowStr);
  //   this.props.editToProduct(data);
  // }


    const { data } = this.state;
    let rowIdx;
    const targetRow = data.find((prod, i) => {
      if (prod.id === row.id) {
        rowIdx = i;
        return true;
      }
      return false;
    });
    if (targetRow) {
      targetRow[fieldName] = value;
      data[rowIdx] = targetRow;
      this.props.editToProduct(targetRow);
      this.setState({ data });
    }
  }

  // update_state(response) {
  //   this.setState({products: response.data});
  // }

  render() {
    return (
      <RemoteCellEdit onCellEdit={ this.onCellEdit } { ...this.state } />
    );
  }
}

class RemoteCellEdit extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const cellEditProp = {
      mode: 'click'
    };
    return (
      <BootstrapTable data={ this.props.data }
                      remote={ true }
                      cellEdit={ cellEditProp }
                      options={ { onCellEdit: this.props.onCellEdit } }
      >
        <TableHeaderColumn dataField='id' isKey={ true }>Product ID</TableHeaderColumn>
        <TableHeaderColumn dataField='nombre'>Product Name</TableHeaderColumn>
        <TableHeaderColumn dataField='precio'>Product Price</TableHeaderColumn>
      </BootstrapTable>
    );
  }
}

// Mapeo la el estado a las propiedades al componente pesentacional
const mapStateToProps = state => {
  return {
    products: state.products
  };
};

// Mapeo la el las acciones a las propiedades al componente pesentacional
const mapDispatchToProps = dispatch => {
  return {
    editToProduct(product){
      dispatch(editToProduct(product));
    }

  };
};

//export default RemoteStoreCellEdit;

export default connect(mapStateToProps, mapDispatchToProps)(RemoteStoreCellEdit) ;
