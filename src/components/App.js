// Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
//import Header from './base/Header_test';
import Header from './base/Header';
import Footer from './base/Footer';
import Product from './section/Product';
import Service from './section/Service';
//import Content from './Global/Content';
//import Footer from './Global/Footer';

// Data
//import items from '../data/menu';

//css
import './css/bootstrap.min.css';
import './css/landing-page.css';
import './font-awesome/css/font-awesome.min.css';
import './css/google_font.css'
import './css/3-col-portfolio.css';
import './css/logo-nav.css';
import './css/ihover.min.css';
//import './css/full-slider.css';
import './css/mi.css';

// import $ from './js/jquery.js';
// import './js/bootstrap.min.js';

class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  };

  render() {
    const { children } = this.props;

    return (

      <div className="App">
        <Header title="Render mi codigo"/>        
        <Service/>
        
        <Footer/>
        <script src="/js/jquery.js"></script>        
        <script src="/js/bootstrap.min.js"></script>
        
        
        
       
      </div>
    );
  }
}

export default App;
