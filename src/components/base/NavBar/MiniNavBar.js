import React from 'react';

import { Glyphicon, Button} from 'react-bootstrap';


const MiniNavBar =  (props) => {
  //<!-- Sidebar Holder -->
  return (
    <nav className="navbar navbar-default">
      <div className="container-fluid">

        <div className="navbar-header">
          <button type="button" id="sidebarCollapse" className="btn btn-info navbar-btn">
            <i className="glyphicon glyphicon-align-left"></i>
            <span>Toggle Sidebar</span>
          </button>
        </div>

        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul className="nav navbar-nav navbar-right">
            <li><a href="#">Page</a></li>
            <li><a href="#">Page</a></li>
            <li><a href="#">Page</a></li>
            <li><a href="#">Page</a></li>
          </ul>
        </div>
      </div>
    </nav>
  );
};


export default MiniNavBar;
