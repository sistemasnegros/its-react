import React from 'react';
import { Glyphicon, Button } from 'react-bootstrap';

import { Link } from 'react-router-dom';

import menus from '../../../const/menu';

import { showMenu }  from '../../../actionCreators';
import {connect } from 'react-redux';

import EllipsisText  from 'react-ellipsis-text';

const menuColapse = (element, props) =>{
  return (
    <div>
      <a href={element.link} data-toggle={"collapse"} aria-expanded="false" >
        <i className={element.icon}></i>
        {element.nombre}
      </a>
      <ul className="collapse list-unstyled" id={element.id}>
        {element.subMenu.map(subMenu =>
          <li key={subMenu.nombre}>
            <Link to={subMenu.link} >
              <EllipsisText text={subMenu.nombre} length={ props.isActive ? (12):(20)} />
            </Link>
          </li>
        )}
      </ul>
    </div>
  );
};

const menuSimple = (element) =>{
  return(
    <Link to={element.link}>
      <i className="glyphicon glyphicon-briefcase"></i>
      {element.nombre}
    </ Link>
  );
};

const NavBar =  (props) => {

  //<!-- Sidebar Holder -->
  return (
    <nav id="sidebar" className={props.isActive}>
      <div id="nav-container" className={props.isActive}>
        <div className="sidebar-header centro">
          <h3>Menu</h3>
          <strong>BS</strong>
          <Button id="sidebarCollapse" onClick={() => props.showMenu("main")}>
            <Glyphicon glyph="menu-hamburger"/>

          </Button>
        </div>

        <ul className="list-unstyled components">
          <li className="active">
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">
              <i className="glyphicon glyphicon-home"></i>
                        Home
            </a>
            <ul className="collapse list-unstyled" id="homeSubmenu">
              <li><a href="#">Home 1</a></li>
              <li><a href="#">Home 2</a></li>
              <li><a href="#">Home 3</a></li>
            </ul>
          </li>

          {menus.map(menu =>
            <li key={menu.nombre}>
              {menu.subMenu ? (
                menuColapse(menu, props)
              ) : (
                menuSimple(menu)
              )}

            </li>
          )}
        </ul>

        <ul className="list-unstyled CTAs">
          <li><a href="https://bootstrapious.com/tutorial/files/sidebar.zip" className="download">Download source</a></li>
          <li><a href="https://bootstrapious.com/p/bootstrap-sidebar" className="article">Back to article</a></li>
        </ul>
      </div>
    </nav>



  );

};

// Mapeo la el estado a las propiedades al componente pesentacional
const mapStateToProps = state => {
  return {
    isActive: state.isActive
  };
};

// Mapeo la el las acciones a las propiedades al componente pesentacional
const mapDispatchToProps = dispatch => {
  return {
    showMenu(element){
      dispatch(showMenu(element));
    }

  };
};


export default connect(mapStateToProps, mapDispatchToProps)(NavBar) ;

//export default NavBar;
