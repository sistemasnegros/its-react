import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import logo from '../images/logo.svg';
//import logo from '../images/radioactive.svg';
import '../css/Header.css';

class Header extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired    
    };
    render() {
        const { title } = this.props;
        return (      
            <header id="myCarousel" className="carousel slide">
         
                <ol className="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
         
                <div className="carousel-inner">
                    <div className="item active">
                 
                        <div className="fill fill-1" ></div>
                 
                        <div className="carousel-caption">
                            {/*  <h2>Caption 1</h2> --> */}
                            <div className="intro-message">
                                <h1>QUE TAN SEGURA ES TU EMPRESA?</h1>
                                <h3>A Template by Start Bootstrap</h3>
                                <hr className="intro-divider"/>
                                <ul className="list-inline intro-social-buttons">
                                    <li>
                                        <a href="https://twitter.com/SBootstrap" className="btn btn-default btn-lg"><i className="fa fa-twitter fa-fw"></i> <span className="network-name">Twitter</span></a>
                                    </li>
                                    <li>
                                        <a href="https://github.com/IronSummitMedia/startbootstrap" className="btn btn-default btn-lg"><i className="fa fa-github fa-fw"></i> <span className="network-name">Github</span></a>
                                    </li>
                                    <li>
                                        <a href="#" className="btn btn-default btn-lg"><i className="fa fa-linkedin fa-fw"></i> <span className="network-name">Linkedin</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                 
                        <div className="fill fill-1"></div>
                        <div className="carousel-caption">
                            {/*  <h2>Caption 1</h2> --> */}
                            <div className="intro-message">
                                <h1>“Creemos que todo el mundo tiene derecho a vivir sin miedo al cibercrimen.”</h1>
                                <hr className="intro-divider"/>
                                <h3> Eugene Kaspersky 
                                    <br/>
                         Director ejecutivo y presidente de Kaspersky Lab</h3>
                                <ul className="list-inline intro-social-buttons">
                                    <li>
                                        <a href="https://twitter.com/SBootstrap" className="btn btn-default btn-lg"><i className="fa fa-twitter fa-fw"></i> <span className="network-name">Twitter</span></a>
                                    </li>
                                    <li>
                                        <a href="https://github.com/IronSummitMedia/startbootstrap" className="btn btn-default btn-lg"><i className="fa fa-github fa-fw"></i> <span className="network-name">Github</span></a>
                                    </li>
                                    <li>
                                        <a href="#" className="btn btn-default btn-lg"><i className="fa fa-linkedin fa-fw"></i> <span className="network-name">Linkedin</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                 
                        <div className="fill fill-1"></div>
                        <div className="carousel-caption">
                            <h2>Caption 3</h2>
                        </div>
                    </div>
                </div>
         
                <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span className="icon-prev"></span>
                </a>
                <a className="right carousel-control" href="#myCarousel" data-slide="next">
                    <span className="icon-next"></span>
                </a>
            </header>    
        );
    }
}




export default Header;
