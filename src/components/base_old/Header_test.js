import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import logo from '../images/logo.svg';
//import logo from '../images/radioactive.svg';
import '../css/Header.css';

class Header extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired    
    };
    render() {
        const { title } = this.props;
        return (      
            <h1> yo soy el header {title}</h1>
        );
    }
}




export default Header;
