// Dependencies
import React from 'react';
import { Route, Switch } from 'react-router-dom';

// Components
//import App from './components/App';

import AppEmpty from './components/AppEmpty';

//import About from './components/About';
//import Contact from './components/Contact';

//import Home from './components/Home';

import Page404 from './components/Page404';

//Mis componentes
import Example from './components/example/Example';
import Formulario from './components/example/Formulario';
import FormLogin from './components/example/FormLogin';
import AxiosTest from './components/example/AxiosTest';
import DataTable from './components/example/DataTable';
import RealtimeTable from './components/example/RealtimeTable';

import EcomerceView from './components/example/ecomerce/EcomerceView';

import RealtimeTablePro from './components/example/RealtimeTablePro';
// <Route exact path="/about" component={About} />
// <Route exact path="/contact" component={Contact} />

// <Route exact path="/" component={Home} />

import store from './store';
import { Provider } from 'react-redux';
import { loadProducts } from './actionCreators';
// Componentes de pruebas
const Home = () => <h2>Este Componente home</h2>;

// Aqui se llama la accion load product
store.dispatch(loadProducts());

// conectando real Realtime
import openSocket from 'socket.io-client';
import  { BASE_URL } from './const';
const socket = openSocket(BASE_URL);

const AppRoutes = () =>
  <Provider store={store}>
    <AppEmpty>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/example" component={Example} />
        <Route exact path="/Formulario" component={Formulario} />
        <Route exact path="/FormLogin" component={FormLogin} />
        <Route exact path="/AxiosTest" component={AxiosTest} />
        <Route exact path="/DataTable" component={DataTable} />
        <Route exact path="/RealtimeTable" component={RealtimeTable} />

        <Route exact path="/Ecomerce" component={EcomerceView} />
        <Route exact path="/RealtimeTablePro" component={RealtimeTablePro} />

        <Route component={Page404} />
      </Switch>
    </AppEmpty>
  </Provider>;


export default AppRoutes;
