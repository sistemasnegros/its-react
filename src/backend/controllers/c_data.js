import express from 'express';
import pool from '../models/modelo_principal';

let router = express.Router();


// Recibe los datos del post
router.get('/get-data', (req, res) =>{



  pool.getNewAdapter(db => {
    db.get('data', (err, results, fields) => {
      if (err){
        console.log(err);
        res.json({});
        db.releaseConnection();

      } else if(results.length > 0) {
        // Iniciar session

        res.json(results);
        db.releaseConnection();

      } else{
        res.json({});
        db.releaseConnection();
      }

      //console.log(results);
      //db.releaseConnection();
      // do not do anything with db that has been released.
    });
  });


});

// Recibe los datos del post
router.post('/create-data', (req, res) =>{


  save_db(req);

  res.json({insert:"ok"});



});


// Recibe los datos del post
router.post('/update-data', (req, res) =>{
  update_db(req);
  res.json({update:"ok"});
});


function update_db(req) {
  const condicion = {
    id:req.body.id
  };
  const newData = {
    nombre: req.body.nombre,
    precio: req.body.precio,
    imagen: req.body.imagen,

  };

  pool.getNewAdapter(db => {
    db
      .where(condicion)
      .update('data', newData, function(err) {
        if (!err) {
          console.log('dato actulizado Updated!');
          db.releaseConnection();
        }else{
          console.log(err);
          db.releaseConnection();
        }
      });
  });
}



function save_db(req) {

  var data = {
    nombre: req.body.nombre,
    precio: req.body.precio,

  };

  pool.getNewAdapter(db => {
    db.insert('data', data, (err, info) => {
      //console.log('New row ID is ' + info.insertId);

    });
  });

}



export default router;
