// Dependencies
import express from 'express';
import webpack from 'webpack';
import path from 'path';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import exphbs from 'express-handlebars';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
//import open from 'open';

// Webpack Configuration
import webpackConfig from '../../../webpack.config.dev';

// Helpers
import * as hbsHelper from '../helpers/handlebars';

// Controllers
import c_general from '../controllers/c_general';
import c_data from '../controllers/c_data';

// Utils
import {
  isMobile
} from '../../lib/basic';

// Server Port
const port = 3000;

// Express app
const app = express();

//oi
import http from 'http';
import socket from 'socket.io';
import {socketEvents} from '../socket';


const server = http.createServer(app);

const io = socket.listen(server);


// le cargo los eventos al sockets
socketEvents(io);


//Peticiones post
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Public
app.use(express.static(path.join(__dirname, '../public')));

// Handlebars setup
app.engine('.hbs', exphbs({
  extname: '.hbs',
  helpers: hbsHelper
}));

// View Engine Setup
app.set('views', path.join(__dirname, "../views"));
app.set('view engine', '.hbs');

// Webpack Compiler
const webpackCompiler = webpack(webpackConfig);

app.use(webpackDevMiddleware(webpackCompiler));
app.use(webpackHotMiddleware(webpackCompiler));

// // Sending all the traffic to React
// app.get('*', (req, res) => {
//   res.sendFile(path.join(__dirname, '../public/index.html'));
// });

// Device detector
app.use((req, res, next) => {
  res.locals.isMobile = isMobile(req.headers['user-agent']);
  return next();
});




// API dispatch
// Cargando controladores
app.use('/api', c_general);
app.use('/api', c_data);
//app.use('/api/blog', blogApi);
//app.use('/api/library', libraryApi)

// Sending all the traffic to React
app.get('*', (req, res) => {
  res.render('index', {
    layout: false
  });
});

// Listen port 3000
//app.listen(port, err => {
server.listen(port, err => {
  if (!err) {
    //open(`http://localhost:${port}`);
  }
});
