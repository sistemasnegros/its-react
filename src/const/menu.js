const menu = [
  {
    nombre : "Inicio",
    icon: "glyphicon glyphicon-home",
    link: "/",
    id: "subConfiguracion",
    subMenu:null
  },{
    nombre : "Config",
    icon: "glyphicon glyphicon-home",
    link: "#subConfiguracion",
    id: "subConfiguracion",

    subMenu:[
      {
        nombre : "Usuarios",
        icon: "home",
        link: "#",
      },
    ]
  },{
    nombre : "Ejemplos",
    icon: "glyphicon glyphicon-home",
    link: "#subEjemplos",
    id: "subEjemplos",

    subMenu:[
      {
        nombre : "example",
        icon: "home",
        link: "/example",
      },{
        nombre : "Formulario",
        icon: "home",
        link: "/Formulario",
      },{
        nombre : "FormLogin",
        icon: "home",
        link: "/FormLogin",
      },{
        nombre : "AxiosTest",
        icon: "home",
        link: "/AxiosTest",
      },{
        nombre : "DataTable",
        icon: "home",
        link: "/DataTable",
      },{
        nombre : "RealtimeTable",
        icon: "home",
        link: "/RealtimeTable",
      },{
        nombre : "Ecomerce",
        icon: "home",
        link: "/Ecomerce",
      },{
        nombre : "RealtimeTablePro",
        icon: "home",
        link: "/RealtimeTablePro",
      }
    ]
  }

];


export default menu;
