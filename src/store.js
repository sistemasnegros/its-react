import {createStore, applyMiddleware } from  'redux';
import thunk from 'redux-thunk';




const reducer = (state, action) => {
  if (action.type === "REPLACE_PRODUCTS") {
    return {
      ...state,
      products: action.products
    };

  }

  if (action.type === "EDIT_TO_PRODUCT") {

    //const newage = 11;
    const newData = state.products.map(obj => {
      // clone the current object
      let newObj = Object.assign({}, obj);
      // update the new object
      if (newObj.id === action.product.id) newObj = action.product;
      return newObj;
    });

    return {
      ...state,
      products: newData
    };
  }

  if (action.type === "ADD_TO_CART") {
    return {
      ...state,
      cart: state.cart.concat(action.product)
    };

  }



  if (action.type === "REMOVE_FROM_CART") {
    return {
      ...state,
      cart: state.cart.filter(product => product.id !== action.product.id)
    };
  }


  if (action.type === "SHOW_MENU") {
    let  isActive = "";
    const setClassName = "active";


    if (action.element === "main") {

      if (state.isActive === setClassName){
        isActive = "";
      }else{
        isActive = setClassName;
      }

    }
    else if (action.element === "subMenu") {

      if (state.isActive === setClassName){
        isActive = setClassName;
      }

      if (state.isActive === setClassName) {
        isActive = "";
      }
    }




    return {
      ...state,
      isActive: isActive
    };
  }

  return state;

};

const logger = store => next => action => {
  console.log('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState());
  return result;
};



export default createStore(reducer, { cart:[], products:[], isActive: ""  }, applyMiddleware(logger, thunk) );
